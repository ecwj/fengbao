import 'package:flutter/material.dart';
import 'package:http/http.dart';
import "dart:async";
import 'dart:convert';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

void main() async {
  await dotenv.load(fileName: ".env");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: dotenv.env['APPNAME'].toString(),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: ''),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  List urls = dotenv.env['URL'].toString().split(",");
  final appname = dotenv.env['APPNAME'].toString();
  // List urls = [
  //   'https://caiking666.com',
  //   'https://caiking777.com',
  // ];
  List targetUrls = [];
  int init = 0;
  String finalUrl = '';
  var isDone = false;
  var failed = false;
  dynamic image = AssetImage('images/bg.jpg');

  @override
  void initState() {
    super.initState();
  }

  Future<void> makePostRequest() async {
    final headers = {
      "Content-type": "application/json",
      "apikey": "eP5mXq9ngFn6UZVLeFzi"
    };

    var i = 0;
    var max = urls.length;
    while (isDone == false) {
      // print('i:${i}');
      if (i > (max - 1)) {
        setState(() {
          failed = true;
          isDone = true;
          finalUrl = '';
        });
      } else {
        var url = urls[i];
        url = Uri.parse('${url}/api.php/Appurl/all');
        // print(url);
        try {
          final response = await post(url, headers: headers);
          // print(response.body);
          if (response.statusCode == 200) {
            setState(() {
              targetUrls = jsonDecode(response.body);
              finalUrl = urls[i].toString();
              init = 1;
              isDone = true;
              image = NetworkImage("${urls[i]}/assets/appbg.jpg");
            });
          } else {
            setState(() {
              finalUrl = '';
              isDone = true;
            });
            i++;
          }
        } catch (exception) {
          i++;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (init == 0) {
      makePostRequest();
    }
    // print(targetUrls);
    // print(finalUrl);
    var deco;
    var buttons;
    if (finalUrl != '') {
      // print(image);
      // print("${finalUrl}/assets/appbg_gj.jpg");
      deco = new BoxDecoration(
        image: DecorationImage(
          image: image,
          onError: (Object exception, StackTrace? stackTrace) {
            print(exception);
            setState() {
              image = AssetImage('images/bg.jpg');
            }

            ;
          },
          fit: BoxFit.cover,
        ),
      );
      // print('length: ${targetUrls.length}');
      if (targetUrls.length == 1) {
        return WebView(url: targetUrls[0], image: image);
      }
      buttons = new GridView.count(
        childAspectRatio: 1.7, //targetUrls.length == 1 ? 3 : 3/2,
        shrinkWrap: true,
        crossAxisCount: targetUrls.length == 1 ? 1 : 2,
        children: targetUrls.map((item) {
          var idx = (targetUrls.indexOf(item) + 1).toString();
          var buttonHeight =
              dotenv.env['BUTTON_HEIGHT_${dotenv.env['APPNAME']}'];
          if (buttonHeight == null) {
            buttonHeight = '120';
          } else {
            buttonHeight = buttonHeight.toString();
          }
          var buttonWidth = dotenv.env['BUTTON_WIDTH_${dotenv.env['APPNAME']}'];
          if (buttonWidth == null) {
            buttonWidth = '120';
          } else {
            buttonWidth = buttonWidth.toString();
          }
          return MaterialButton(
            textColor: Colors.white,
            splashColor: Colors.greenAccent,
            elevation: 8.0,
            child: Container(
              height: double.parse(buttonHeight),
              width: double.parse(buttonWidth),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('images/' +
                        dotenv.env['BUTTON_${dotenv.env['APPNAME']}']
                            .toString()),
                    fit: BoxFit.cover),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Center(
                  child: Text('线路 ' + idx,
                      style: TextStyle(
                        fontSize: 20,
                      )),
                ),
              ),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => WebView(url: item, image: image),
                ),
              );
            },
          );
        }).toList(),
      );
    } else {
      deco = new BoxDecoration(
        color: Colors.black,
      );
      var buttonHeight = dotenv.env['BUTTON_HEIGHT_${dotenv.env['APPNAME']}'];
      if (buttonHeight == null) {
        buttonHeight = '120';
      } else {
        buttonHeight = buttonHeight.toString();
      }
      var buttonWidth = dotenv.env['BUTTON_WIDTH_${dotenv.env['APPNAME']}'];
      if (buttonWidth == null) {
        buttonWidth = '120';
      } else {
        buttonWidth = buttonWidth.toString();
      }
      buttons = new MaterialButton(
        padding: EdgeInsets.all(8.0),
        textColor: Colors.white,
        splashColor: Colors.greenAccent,
        elevation: 8.0,
        child: Container(
          height: double.parse(buttonHeight),
          width: double.parse(buttonWidth),
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/' +
                    dotenv.env['BUTTON_${dotenv.env['APPNAME']}'].toString()),
                fit: BoxFit.cover),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Text(
                '暂时没有可用线路',
                style: TextStyle(
                  fontSize: 20,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        // ),
        onPressed: () {},
      );
    }
    var margin = dotenv.env['LOGO_MARGIN_${dotenv.env['APPNAME']}'];
    if (margin == null) {
      margin = '180';
    } else {
      margin = margin.toString();
    }
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(widget.title),
      //   backgroundColor: Colors.black,
      // ),
      body: Container(
          decoration: deco,
          child: Center(
            child: Column(children: [
              Container(
                height: double.parse(margin),
                margin: EdgeInsets.fromLTRB(20, 70, 20, 20),
                // child: Image(image: AssetImage('images/logo_${appname}.png')),
              ),
              buttons,
            ]),
          )),
    );
  }
}

class WebView extends StatelessWidget {
  WebView({Key? key, required this.url, required this.image}) : super(key: key);
  final String url;
  final image;
  final GlobalKey _parentKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: Container(
        margin: const EdgeInsets.only(top: 0.0),
        child: SafeArea(
          top: true,
          child: Stack(
            key: _parentKey,
            children: [
              new WebviewScaffold(
                url: url,
                withZoom: true,
                withLocalStorage: true,
                mediaPlaybackRequiresUserGesture: false,
                hidden: false,
                initialChild: Container(
                  decoration: BoxDecoration(
                    // color: Colors.black,
                    image: DecorationImage(
                      image: AssetImage('images/bg.jpg'), //image,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
